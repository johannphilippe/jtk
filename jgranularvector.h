#ifndef JGRANULARVECTOR_H
#define JGRANULARVECTOR_H

#include<iostream>
#include<cstring>
#include<array>
#include<memory>
#include<algorithm>
#include"jio.h"

//This vector stores a pointer to arrays. It allocates several arrays of size block_size_, and then iterates through it


//Faster than JVector for complex data types, slower for simple data - primitive etc.
//although, not that faster

using namespace JTK;

template<typename T>
class JGranularVector {

public:
    JGranularVector(std::size_t initial_size = 0, std::size_t bsize = 10000) :
        size_(0),
        capacity_(initial_size),
        block_size_(bsize),
        number_of_vectors(capacity_ / block_size_)
    {
        data_ = new T*[number_of_vectors];
        for(int i =0; i < number_of_vectors; i++)
        {
            data_[i] = new T[block_size_];
        }
    }

    //copy constructor
    JGranularVector(const JGranularVector& that) :
        size_(that.size()),
        capacity_(that.capacity()),
        block_size_(that.block_size()),
        number_of_vectors(that.numberOfVectors())
    {
        data_ = new T*[number_of_vectors];
        for(int i =0; i < number_of_vectors; i++)
        {
            std::memcpy(data_[i], that.data_[i], block_size_);
        }

    }

    //move constructor
    JGranularVector(JGranularVector&& that):
        size_(that.size()),
        capacity_(that.capacity()),
        block_size_(that.block_size()),
        number_of_vectors(that.numberOfVectors())
    {
        data_ = that.data_;
        that.reset();
    }


    void reserve(std::size_t nsize)
    {
        if(nsize > capacity_)
        {
           std::size_t new_nbr_of_vector = (nsize / block_size_);
           T **tmp = new T*[new_nbr_of_vector];

           for(std::size_t i = 0; i < number_of_vectors; i++) {
               tmp[i] = data_[i];
           }

           for(std::size_t i =number_of_vectors; i < new_nbr_of_vector; i++)
           {
               data_[i] = new T[block_size_];
           }

           capacity_ = nsize;
           number_of_vectors = new_nbr_of_vector;
        }
    }

    void resize(std::size_t nsize)
    {
        if(nsize != size_) {
            std::size_t new_nbr_of_vectors = (nsize / block_size_);
            T **tmp = new T*[new_nbr_of_vectors];

            for(std::size_t i=0; i < new_nbr_of_vectors; i++) {
                tmp[i] = new T[block_size_];
                if(i < number_of_vectors) { // then copy
                    std::size_t passed = i * block_size_;
                    std::size_t number_of_items = size_ - passed;
                    std::size_t to_copy = (number_of_items >= block_size_) ? block_size_ : number_of_items;

                    if(to_copy > 0 )std::memcpy(tmp[i], data_[i], to_copy);
                }
            }
            delete[] data_;
            data_ = tmp;
            size_ = nsize;
            capacity_ = nsize;
            number_of_vectors = new_nbr_of_vectors;
        }

    }

    void push_back(T val)
    {
        if(size_ +1 > capacity_)  // reallocates
        {
            T ** tmp = new T*[number_of_vectors +1 ];
            for(std::size_t i = 0; i <= number_of_vectors; i++)
            {
                tmp[i] = new T[block_size_];
                std::size_t number_of_items = size_ - (i * block_size_);
                std::size_t to_copy = ( number_of_items >= block_size_ ) ? block_size_ : number_of_items;

                if(to_copy > 0) std::memcpy(tmp[i], data_[i], to_copy);
            }
            delete[] data_;
            data_ = tmp;
        }

        data_[size_/block_size_][size_ % block_size_] = val;
        size_++;
    }

    T &operator[] (int index)
    {
        return data_[index/block_size_][index % block_size_];
    }


    std::size_t size() {return size_;}
    std::size_t block_size() {return block_size_;}
    std::size_t capacity() {return capacity_;}
    std::size_t numberOfVectors() {return number_of_vectors;}


    void reset()
    {
        this->size_ = 0;
        this->capacity_ = 0;
        this->block_size_ = 0;
        this->number_of_vectors = 0;
        this->data_ = nullptr;
    }

private:

    std::pair<std::size_t, std::size_t> getAdress(std::size_t index)
    {
        std::pair<std::size_t, std::size_t> res;
        res.first = (int)index / block_size_;
        res.second = index % block_size_;
        return res;
    }

    T **data_;
    std::size_t size_;
    std::size_t capacity_;
    std::size_t block_size_;
    std::size_t number_of_vectors;


};
#endif // JGRANULARVECTOR_H
