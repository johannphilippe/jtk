#ifndef JARRAY_H
#define JARRAY_H

#include<iostream>
#include<algorithm>
#include<memory>
#include<cstring>



// Array without allocation - faster than std::array for small data types
//even faster than C array sometimes (for filling) with complex data
template<typename T, const std::size_t S>
class JArray
{
public:
    JArray(int n = 0) : size_(S)
    {
        std::memset(arr, n, size_);
    }


    JArray(const JArray &that)
    {
       std::memcpy(&this->arr, &that.arr, size_);
    }

    JArray(JArray&& that)
    {
        arr = std::move(that.arr);
    }


    JArray(T src[])
    {
        std::memcpy(&arr,src, size_);
    }



    T &operator[] (int index)
    {
        return arr[index];
    }

    const std::size_t size(){return size_;}





private:
    T arr[S];
    const std::size_t size_;

};


#endif // JARRAY_H
