TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
 QMAKE_CXXFLAGS_RELEASE += -O3
SOURCES += main.cpp

HEADERS += \
    jio.h \
    jvector.h \
    jarray.h \
    jgranularvector.h

#QMAKE_CXXFLAGS += -pg

#LIBS += -pg
