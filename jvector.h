#ifndef CONTAINERS_H
#define CONTAINERS_H
#include<iostream>
#include<cstring>
#include<array>
#include<memory>
#include<algorithm>
#include<iomanip>
#include<utility>
#include<string>

#include"jio.h"
using namespace JTK;

//simple vector class
// block size is reallocation block when push back
// initial size is initial capacity

// seems faster than std::vector for simple data types, not so much for complex/composite.
template<typename T>
class JVector
{
public:
    JVector(size_t initial_size = 0, size_t block_size = 10000) :
        size_(0),
        capacity_(initial_size),
        arr(nullptr),
        block_size_(block_size)
    {
        arr = new T[capacity_];
    }

    //copy semantics
    JVector(const JVector& that) :
        size_(that.size()),
        capacity_(that.capacity()),
        block_size_(that.block_size())
    {
        arr = new T[capacity_];
        std::memcpy(arr, that.data(), size_);
        print("Copy");
    }


    //move semantics
    JVector(JVector&& that)
    {
        arr = std::move(that.arr);
        size_ = that.size();
        block_size_ = that.block_size();
        capacity_ = that.capacity();

        that.reset();
        print("Move");
    }

    ~JVector()
    {
        delete arr;
    }

    void reserve(std::size_t nsize)
    {
        if(nsize > capacity_) {
            capacity_ = nsize;
            T *tmp =new T[capacity_];
            std::memcpy(tmp, arr, size_);
            delete arr;
            arr = tmp;
        }
    }

    void resize(std::size_t nsize)
    {
        if(nsize != size_) {
            T *tmp = new T[nsize];
            std::memcpy(tmp, arr, size_);
            delete arr;
            arr = tmp;
            size_ = nsize;
            capacity_ = nsize;
        }
    }

    void push_back(T val)
    {
        if(size_ +1 > capacity_) { // realloc block
            capacity_+=block_size_;
            T * tmp = new T[capacity_];
            std::memcpy(tmp, arr, size_);
            delete arr;
            arr = tmp;
        }
        size_++;
        arr[size_ - 1] = val;
    }


    T &operator[] (int index)
    {
        return arr[index];
    }

    JVector& operator=(JVector that)
    {
        std::swap(arr, that.data());
        return *this;
    }

    JVector& operator=(JVector &&that)
    {
        return *that;
    }



    T &at(std::size_t index) {
        if(index >= size_ || index <0) {
            std::cout << "Array index out of bound" << std::endl;
            exit(0);
        }
        return arr[index];
    }


    std::size_t size() const
    {
        return size_;
    }

    std::size_t capacity() const
    {
        return capacity_;
    }

    std::size_t block_size() const
    {
        return block_size_;
    }

    T *begin() {return &arr[0];}
    T *end() {return &arr[size_];}


    T operator++(int){
        return arr++;
    }

    T *data() const {
        return arr;
    }

    void setBlockSize(size_t n) {
        block_size_ = n;
    }

    //actually clears values, except if values are pointers (only clears pointers)
    void clear()
    {
        delete arr;
        this->reset();
    }

    //does not clear values -- used in move semantics
    void reset()
    {
        arr = nullptr;
        block_size_ = 0;
        size_ = 0;
        capacity_ = 0;
    }

private:
    std::size_t block_size_;
    std::size_t size_;
    std::size_t capacity_;
    T *arr;
};


#endif // CONTAINERS_H
