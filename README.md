# JTK

CPP11 template classes and utilities (containers mostly). It is mostly experiment, and may not be functional in some use cases.

## JVector
A vector-like container that appears to be slightly faster than std::vector, particularly on small values. It holds a simple array, dynamically allocated, and manages 
reallocation itself (when using push_back, resize, or reserve). So, it stores data contiguously.  

## JGranularVector
A vector-like container that stores data in several arrays of ```block_size``` size. This framgentation is not very efficient on simple data types (primitives), but appears to be faster than std::vector and JVector with complex data types (where memory fragmentation is necessary). 

## JArray 
A std::array like array, with static size. It appears to be sometimes faster than std::array, though this implementation is very simple.
It works without dynamic allocation, so doesn't need to explicitely delete its data.
