#ifndef JIO_H
#define JIO_H


namespace JTK
{

    template<typename T>
    static void print(T arg)
    {
        std::cout << arg << std::endl;
    }

    template<typename T, typename... L>
    static void print(T arg1, L... list)
    {
       std::cout << arg1 << "\t";
       print(list...);
    }

}


#endif // JIO_H
