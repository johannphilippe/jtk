#include <iostream>
#include<chrono>
#include<functional>

#include<vector>
#include"jvector.h"
#include"jarray.h"
#include<array>
#include"jio.h"
#include"jgranularvector.h"
using namespace std;
using namespace JTK;


struct Complex{
    int i[1000];
    double z[1000];
};

template<typename T>
void profile(T &var, std::string toprint)
{
    auto t1 = std::chrono::high_resolution_clock::now();

    for(auto &a : var)
    {
        Complex z = a;
    }


    auto t2 = std::chrono::high_resolution_clock::now();
    auto dur1 = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << toprint << " == " << dur1 << std::endl;
}


void profileCustom(std::function<void(void)> f, std::string toprint)
{

    auto t1 = std::chrono::high_resolution_clock::now();

    f();

    auto t2 = std::chrono::high_resolution_clock::now();
    auto dur1 = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << toprint << " == " << dur1 << std::endl;

}




static void vector_profiling()
{
    int N = 100000;

    //VECTOR
    auto t1 = std::chrono::high_resolution_clock::now();
    std::vector<Complex> vec;
    vec.resize(N);
    for(int i = 0; i < N; i++) {
        //vec.push_back(Complex());
        vec[i] = Complex();
    }
    auto t2 = std::chrono::high_resolution_clock::now();


    auto dur1 = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

    std::cout << "Vector dur = " << dur1 << std::endl;




    //ARRAY
    auto t3 = std::chrono::high_resolution_clock::now();


    Complex *arr = new Complex[N];
    for(int i = 0; i < N; i++)
    {
        arr[i] = Complex();
    }

    auto t4 = std::chrono::high_resolution_clock::now();


    auto dur2 = std::chrono::duration_cast<std::chrono::microseconds>(t4 - t3).count();

    std::cout << "Array dur = " << dur2 << std::endl;



    //JVector
    auto t5 = std::chrono::high_resolution_clock::now();

    JVector<Complex> jvec;
    jvec.resize(N);


    for(int i = 0; i < N; i++) {
        //jvec.push_back(Complex());
        jvec[i] = Complex();
    }


    auto t6 = std::chrono::high_resolution_clock::now();
    auto dur3 = std::chrono::duration_cast<std::chrono::microseconds>(t6 - t5).count();

    std::cout << "JVector dur = " << dur3 << std::endl;





    std::cout << "vec len " << vec.size() << std::endl;
    std::cout << "jvec len " << jvec.size() << std::endl;


    std::cout << "vec size : " << sizeof(vec) << std::endl;
    std::cout << "arr size : " << sizeof(arr) << std::endl;
    std::cout << "jvec size : " << sizeof(jvec) << std::endl;


    profile(vec, "Vector profiling");
    profile(jvec, "JVector profiling");



    JVector<Complex> jvec2(std::move(jvec));
    profile(jvec, "jvec");
    profile(jvec2, "jvec2");


    std::vector<Complex> vec2(std::move(vec));
    profile(vec, "vec");
    profile(vec2, "vec2");



    print("JVEC1", jvec.size());
    print("JVEC2", jvec2.size());





    profileCustom([&](){
        JVector<Complex>jvec4(jvec2);
        print(jvec4.size());
    }, "Copy constructor : ");

    profileCustom([&](){
        JVector<Complex>jvec5(std::move(jvec2));
        print(jvec5.size());
    }, "Move constructor : ");


    profileCustom([&](){
        std::vector<Complex> vec4(vec2);
        print(vec4.size());
    }, "Copy on stdvector");

    profileCustom([&](){
        std::vector<Complex> vec4(std::move(vec2));
        print(vec4.size());
    }, "Move on stdvector");

}


static void array_profiling()
{
    const std::size_t N = 100000;


    profileCustom([&](){
        Complex arr[N];
        for(int i =0; i < N; i++) {
            arr[i]=Complex();
        }
    }, "Profile C Array");

    profileCustom([&](){
        std::array<Complex, N> arr;
        for(int i = 0; i < N; i++) {
            arr[i] = Complex();
        }
    }, "Profile Cpp Array");

    profileCustom([&](){
        JArray<Complex, N> jarr;
        for(int i = 0;  i < N;i++)
        {
            jarr[i] = Complex();
        }
    }, "Profile Jarray");

}



static void granular_array_profiling()
{
    const std::size_t N = 50000;

    profileCustom([&](){
        std::vector<Complex> vec(N);
        for(int i =0; i < N; i++)
        {
            vec[i] = Complex();
        }
    }, "Profile vector");


    profileCustom([&](){
        JVector<Complex> jvec(N, 10000);
        for(int i =0; i < N; i++)
        {
            jvec[i] = Complex();
        }
    }, "Profile JVECTOR");


    profileCustom([&](){
        JGranularVector<Complex> jgran(N, 10000);
        for(int i =0; i < N; i++)
        {
            jgran[i] = Complex();
        }
    }, "Profile JGRANULAR");



    profileCustom([&](){
        std::vector<int> vec(N);
        for(int i =0; i < N; i++)
        {
            vec[i] = i;
        }
    }, "Profile vector");


    profileCustom([&](){
        JVector<int> jvec(N, 10000);
        for(int i =0; i < N; i++)
        {
            jvec[i] = i;
        }
    }, "Profile JVECTOR");


    profileCustom([&](){
        JGranularVector<int> jgran(N, 10000);
        for(int i =0; i < N; i++)
        {
            jgran[i] = i;
        }
    }, "Profile JGRANULAR");
}



int main()
{

    granular_array_profiling();


    return 0;
}



